---
title: Eindelijk designen!
date: 2017-10-09
---

### Maandag 09/10/17

* Verder uitwerken literatie 2
* Design app gemaakt
* Detail bespreking

Vandaag zijn we literatie 2 gaan afronden. Hierbij heb ik het design van de app gemaakt, en zijn we de details gaan bespreken voor de expo van na de vakantie.
