---
title: Eerste studiodag
date: 2017-11-15
---

### Woensdag 15/11/17

* Eerste studiedag
* Gebrainstormed
* team-afspraken & kwaliteiten besproken

Vandaag hebben we de erste brainstorm gedaan. We begonnen met het analyseren van de kwaliteiten van het team. Dit heb ik netjes opgeschreven zodat we deze goed kunnen mee nemen in de opdrachten.

We hebben ook team afspraken gemaakt en deze opgeschreven op een lijst.

De eerste brainstorm hebben we gehad. We hebben de eerste ideeen voor het onderzoek gedaan. Namelijk: Onderzoek op Internet, Interview en een Enquette maken en delen. Om al ons gemaakte werk op te slaan heb ik een google drive gemaakt voor de groep om zo alles overzichtelijk te delen met elkaar.

Tijdens de brainstorm heb ik in de vlugheid ook even een logo gemaakt voor het team..