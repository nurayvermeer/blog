---
title: Eerste les
date: 2017-09-04
---

### Maandag 04/09/17

* Team poster maken
* taken & regels bespreken
* planning & taakverdeling

Vandaag hebben we een poster gemaakt op papier, dit door de inventarisatie van het team. We bespraken elkaars kwaliteiten en ambities.

We hebben een planning gemaakt en doorgenomen voor week 1 & 2 en de taakverdeling. Deze hebben we goed doorgenomen.

Per tweetal hebben we een doelgroep gekozen en deze onderzocht, en een onderwerp/thema wat bij rotterdam past.

Flors en ik hadden Maritiem als thema gekozen en als doelgroep CMGT.

Uiteindelijk met zijn we met z’n alle tot de conclusie gekomen qua onderwerp/thema en doelgroep waar we mee verder willen gaan. Namelijk; Thema: Architectuur en doelgroep: Informatica.
