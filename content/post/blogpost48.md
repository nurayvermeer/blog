---
title: CONCLUSIE periode 2
date: 2018-01-18
---

### Woensdag 18/01/18

Vandaag heb ik mijn leer dossier afgemaakt en daarbij de conclusie van deze 
periode geschreven.

## Conclusie periode 2

Ik heb de slag weer een beetje te pakken denk ik. 
Ik heb weer geleerd dat ik constant vooruit moet kijken naar leerdoelen, 
omdat ik anders snel gedemotiveerd raak. Ik vind dat ons groepje ondanks de 
tegenslagen, en alles op het laatste moment nog veranderen het heel goed 
heeft gedaan. De 1 deed harder zijn best dan de ander (liep harder voor 
het project). Ik denk dat dit enigzins toch wel wat spanningen veroorzaakte. 
De volgende periode wil ik me niet alleen op visualiseren focussen, ik wil 
wat anders leren waar ik misschien wel minder goed in ben. 