---
title: Presentatie literatie 2
date: 2017-10-04
---

### Woensdag 04/10/17

* Presentatie literatie 2
* Feedback verwerkt
* Verbeter punten besproken

Vandaag hebben de we de presentatie van literaire 2 gehad. Dit ging uiteindelijk minder goed dan verwacht. Door de feedback en vragen die we kregen bleek eigenlijk voor de doelgroep en eind gebruiker nog veel onduidelijk te zijn. Hier hebben we uiteindelijk over gepraat en al verbeter punten opschreven waar we de komende tijd aan gaan werken.
