---
title: Nog een week en dan is de Expo
date: 2018-01-10
---

### Dinsdag 10/01/18

* Ziek naar huis (extreme rugpijn)
* Nieuw concept idee

We zijn begonnen met het voorbereiden van de expo. Ik ben begonnen aan het 
high-fid prototype. In de middag zijn Quinty en Rosa naar het validatie moment 
van de Designrationale geweest. Hun kregen te horen dat de Designrationale niet 
overeenkomt met ons concept. Hij gaf als tip om het concept aan te passen en 
nog eens goed te kijken naar het probleem. Het was echt niet leuk om te horen 
dat hij dus niet was goedgekeurd en dat een week voor de expo nog werd 
geadviseerd om het concept te veranderen. Hun hebben een alumni erbij gehaald 
om te vragen of hij nog tips had. Quinty en Rosa zijn die dag op een nieuw idee 
gekomen om te doen. Dit hebben ze doorgeappt omdat blijkbaar de rest van het 
groepje ook oppeens weg was gegaan. Iedereen was er enthousiast over. Dus hopen 
dat we iets goeds kunnen presenteren op de expo.