---
title: Engels ondersteunings-module
date: 2017-11-17
---

### Vrijdag 17/11/17

* Eerste keer Engels

Vandaag voor het eerst de ondersteunings-module Engels gehad. 
Dit viel gelukkig wel mee, het was leuk en we hebben een fijne leraar die 
heel goed Engels spreekt.