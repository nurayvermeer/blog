---
title: KICK-OFF periode 3
date: 2018-02-06
---

### Dinsdag 06/02/18

Vandaag was de kick-off van periode 3. We hebben een hele lange presentatie 
gehad op school over het onderwerp.  Het wordt een soort gesplitst semeseter. 
Met hetzelfde onderwerp maar 2 verschillende doelgroepen. Periode 3 is onze 
doelgroep studenten van de HR, en periode 4 jongeren uit Rotterdam Zuid. 
De bedoeling is dat we een oplossing creeren om het leefbaarder te maken en 
een gezondere leeftstijl te creeren voor de doelgroep.