---
title: EXPO & CONCLUSIE OP4
date: 2018-06-01
---

Vandaag was de beeedgeing, ik heb met de groep contact gehouden over de expo hoe alles ging. 

### CONCLUSIE

Ik ben zeer tevreden over hoe deze periode is verlopen, het gehele project en 
de samenwerking. De samenwerking in vergelijking met vorige periode is echt 
super gegaan. Ik denk dat dat ook komt doordat we ons goed hadden voorbereid 
omdat we maar met drie personen verder gingen. Het enige wat ik niet vond lopen 
is de drive. Dat maakt me wel nerveus, vooral op het laatste moment. Maar ja 
het is geen wereld ramp, ik hoop dat dit de volgende keer beter geregeld kan 
worden. Door bijvoorbeeld een persoon daar toe aan te zetten. Ik heb me deze 
periode weer wat meer ontwikkeld en weer nieuwe dingen geleerd. De feedback 
die ik kreeg bij de competentie tafels heb ik meegenomen deze periode om 
mijzelf daarin te verbeteren. Naar mijn inzien is dat gelukt. Ik zou graag 
de aankomende feedback goed willen verwerken zodat ik volgend schooljaar 
direct weet op welke punten ik verder kan gaan en waar ik mij nog moet 
verbeteren. Zo hoop ik volgend jaar weer een frisse start te kunnen maken. 
Gelukkig is deze periode alles met mijn blog bijhouden wel goed gegaan.
