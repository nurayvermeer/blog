---
title: Woensdag Stuidiodag
date: 2018-05-30
---

### Woensdag 30/05/18

Vandaag hebben we de puntjes op de i gezet en nog dingen laten vailideren  die 
we nog moesten doen. Ook heb ik met mijn team en Bob besproken dat ik vrijdag 
helaas niet bij de expo kan zijn wegens de beeedeging van mijn vriend. Ik vond 
het moeilijk om deze keuze te maken, omdat ik niet mijn twee teamgenoten met 
de expo wilden laten zitten. Maar nadat ik het besproken had, zeiden ze beide 
dat die beediging belangrijk voor me was en dat ze me op de hoogte zouden 
houden van de expo. Dus dat was wel fijn.