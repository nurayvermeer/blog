---
title: Maastricht
date: 2018-02-09
---

### Woensdag t/m vrijdag in Maastricht

De afgelopen 3 dagen zijn we in Maastricht geweest met CMD. 
We hebben het hier erg leuk gehad en we zijn alvast een beetje bezig geweest 
met het onderwerp van het semester. We hebben eigenlijk alle opleveringen in 
twee dagen gedaan. Een soort mini project. We kregen een doelgroep en moesten 
met het onderwerp een gezondere leefstijl de straat op om onderzoek te doen. 
Dat onderzoek moesten we in kart brengen en toen een prototype maken 
en exposeren. 