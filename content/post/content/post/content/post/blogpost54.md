---
title: Her-Validatie
date: 2018-02-21
---

### Woensdag studiodag

Naar aanleiding van de validatie hebben we de plan van aanpak uitgebreider 
gemaakt, en een overzichtelijke planning gemaakt voor deze periode. Vandaag 
heb ik ook een beschrijving gemaakt over het nut van een planning. Ook heb ik 
vandaag een stakeholdersmap gemaak over studenten op CMI van het HR. Dit 
gaat over invloeden van buitenaf in een gezonde leefstijl en invloeden van 
personen in de directe omgeving. Ook heb ik vandaag snel even een logotje 
gemaakt voor de groep.