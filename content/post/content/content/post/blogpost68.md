---
title: Maandag Studiodag
date: 2018-04-23
---

### Maandag 23/04/18

We hebben onze onderzoeksmethodes opgesteld. We willen zowieso gaan interviewen 
omdat dat een persoonlijk gesprek kan geven. We willen ook een enquette laten 
invullen, omdat dit wat anoniemer is en dan kan je je uitkomsten in 
procenten verwerken. Ook willen we veel tijd besteden aan desk research. 
We zijn er al achter dat er veel in boeken, tijdschriften en op het web te 
vinden is.