---
title: Maandag studiodag
date: 2018-05-14
---

### Maandag 14/05/18

We zijn nu echt gaan synthetiseren wat ons concept is, wil, kan en bereikt. 
Verder hebben we persona's gemaakt en laten valideren om deze er constant 
bij te kunnen pakken. Deze persona's kunnen we in een later stadium nog 
gebruiken omdat het een soort samenveging is van de gebruiker/doelgroep.