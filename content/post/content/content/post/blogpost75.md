---
title: Woensdag studiodag
date: 2018-05-23
---

### Woensdag 23/05/18

Vandaag hebben we besproken wat Mark en Ashley vorige week hebben gedaan. 
We hebben ook gekeken naar wat er nog moet gebeuren en wat we nog gaan maken. 
We moeten nu aan een protoype beginnen om te kunnen gaan testen. Maar ons 
concept is niet tastbaar, dus we moeten toch een creatieve manier/oplossing 
bedenken hoe we dit kunnen aanpakken. Ook hoe we vervolgens het concept op 
de expo gaan presenteren. Zo zijn we gaan schetsen individueel om zo te kijken 
hoe we er allen apart een beeld bij hebben. Een soort plattegrond.