---
title: Expo
date: 2017-10-25
---

### Woensdag 25/10/17

* expo
* les van studiecoach
* werken aan leerdossier
* conclusie maken

Vandaag hebben we de Expo gehad van onze eerste periode. Dit ging best goed. Ondanks alweer de minimale voorbereiding hebben we behoorlijk goed gepresteerd. We kregen veel waardevolle en positieve feedback waar we meer mee kunnen doen de volgende keer. Verder les gehad van Gerhard, die heeft ons dingen uitgelegd over het leerdossier. Ik ben voorin de klas geroepen met Jiska om onze feedback door te nemen met elkaar voor de klas. Verder heb ik ook de afronding van mijn leer dossier gemaakt.
