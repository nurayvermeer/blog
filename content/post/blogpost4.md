---
title: Vierde les
date: 2017-09-07
---

### Donderdag 07/09/17

* Concept
* Paper prototype
* Individueel onderzoek

Vandaag het concept handmatig uitgewerkt met de groep en zo tot een goed idee gekomen van het spel. Daarbij ook ideeën bedacht voor het prototype, daar gaan we maandag mee verder. Verder in de tussentijd nog een beetje aan mijn eigen onderzoek gewerkt.
