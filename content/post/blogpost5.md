---
title: Bloggeeeen
date: 2017-09-11
---

### Maandag 11/09/17

* Persoonlijk onderzoek
* Concept
* Paper prototype
* Blog aanmaken SmartGit

Vandaag het persoonlijke onderzoek afgemaakt. Verder met de groep het concept goed besproken, en doorgenomen. We zijn ook al begonnen aan het paper prototype, van karton. Verder moesten we een blog aanmaken in SmartGit.
