---
title: EXPO
date: 2018-01-17
---

### Woensdag 17/01/18

* Voorbereiden
* Expo
* Last minute poster

Vandaag ben ik weer om 09:00 naar school gegaan om van mijn werk nog wat 
dingetjes te doen. Rond 13:00 waren de rest van het groepje er en hebben wij/ik 
ons voorbereid op de expositie. Nog wat dingen geprint enzo. En toen vonden 
we dat we toch wel weinig presentatie middelen hadden. Maar het was al heel 
laat dus ik heb echt in 10 minuten even snel nog een poster in elkaar gedraaid, 
zodat we nog wat extra's om te laten zien hadden.

Verder ging de expo wel goed. Ons werk was zo slecht nog niet. Vooral
omdat we eigenijk pas een week het difinitieve concept hadden bedacht.