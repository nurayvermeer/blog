---
title: Laatste x evolutieleer
date: 2018-01-16
---

### Dinsdag 16/01/18

* Validatie high-fid Prototype
* Afmaken leerdossier
* Evolutieleer

Vandaag ben ik om 09:00 naar school gegaan om het prototype af te maken. Toen 
ging ik om 13:00 naar de validatie. Ze was erg positief alles klopte en werkte 
op de juiste manier. En ik had een overzichtelijk scenario. Ze heeft hem gelijk 
goed gekeurd. Ik heb nog een paar kleine wijzigingen gedaan eraan die middag 
naar haar advies. Verder ben ik de rest van de middag bezig geweest met mijn 
leerdossier en 's avonds weer naar evolutieleer de laatste les geweest. (helaas)