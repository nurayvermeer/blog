---
title: Maandag 3e studiodag
date: 2017-11-27
---

### Maandag 27/11/17

* Concept bespreking
* Enquette analyse

Vandaag hebben we een groeps bespreking gehad. We hebben wat concept 
ideeen opgeschreven en onderzocht wat de mogelijkheden waren. We hebben wat 
ideeen, alleen we moeten kijken of deze ook in elkaar samen passen.

Verder heb ik de enquette geanalyseerd en hlemaal uitgewerkt in een 
overzichtelijk document. Zo hebben we meer inzicht in de doelgroep gekregen. 
Ik denk dat we alleen nog wel wat persoonlijke vragen moeten stellen 
aan de doelgroep.
