---
title: Scrummen en plannen
date: 2017-11-29
---

### Woensdag 29/11/17

* Scrummen
* Brainstormen
* Enquête analyse

Vandaag hebben we een scrumbord gemaakt en een planning voor de dag. 
We hebben gebrainstormd hoe we van drie ideeen een concept konden maken, 
maar dat was best lastig. We merken dat er niet veel meer duidelijk is over 
de doelgroep dan na de enquête. Ik heb van de enquête een analyse gemaakt. 
Daaruit bleek we de doelgroep wat duidelijkere vragen stellen, 
zoals ik in mijn vorige post al zei. 